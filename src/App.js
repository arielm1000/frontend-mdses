import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./components/Home"
import Login from "./components/auth/Login"
import Registro from "./components/auth/Registro";
import Subirbene from "./components/Subirbene";
import Subirtirillas from "./components/Subirtirillas";
import Buscarbene from "./components/Buscarbene";
import Buscarbenename from "./components/Buscarbenename";
import AlertaContext from './alertas/alertaState';
import AuthContext from './context/authState';
 
function App() {
  return (
    <AuthContext>
    <AlertaContext>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/registro" component={Registro} />
        <Route exact path="/subirbene" component={Subirbene} />
        <Route exact path="/subirtirillas" component={Subirtirillas} />
        <Route exact path="/buscarbene" component={Buscarbene} />
        <Route exact path="/buscarbenename" component={Buscarbenename} />
      </Switch>
    </BrowserRouter>
    </AlertaContext>
    </AuthContext>
  );
}
 
export default App;