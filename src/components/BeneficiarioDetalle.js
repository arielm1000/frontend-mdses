import React, {useState} from 'react';
import { Button, Accordion, Card, Col, Row, Form } from 'react-bootstrap';
import moment from 'moment';
import 'moment/locale/es';
import MaskedFormControl from 'react-bootstrap-maskedinput'
import MaskedInput from 'react-maskedinput'
import { usePDF, Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import { PDFViewer, PDFDownloadLink, Image } from '@react-pdf/renderer';
import logodesydl from '../img/logodesydl.jpg';
import gobtuc from '../img/gobtuc.jpg';

function BeneficiarioDetalle({benef, index, props}) {
    // Para el mes en español
    moment.locale('es');

    // Create styles
    const styles = StyleSheet.create({
        page: {
            flexDirection: 'column',
            backgroundColor: 'white'
        },
        section: {
            flexDirection: 'row',
            backgroundColor: 'white',
        },
        section1: {
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
            height: '20%',
        },
        view: {
            width: '100%',
            height: '100%',
            backgroundColor: 'white',
            margin: 15,
            padding: 10,
            flexGrow: 1
        },
        image: {
            objectFit: 'cover',
        },
        etiqueta: {
            backgroundColor: 'white',
            color: 'black',
            fontSize: 12,
            width: '17%',
            height: '70%',
            margin: '0px 5px 0px 10px',
            padding: '5px',
        },
        datos: {
            backgroundColor: '#D3D3D3',
            color: "black",
            fontSize: 12,
            width: '83%',
            height: '70%',
            margin: '0px 5px 0px 10px',
            padding: '2px',
            borderRadius: '5px',
        },
        etiqueta1: {
            width: '30%',
            height: '15%',
            fontSize: 12,
            margin: '10px',
            padding: '6px',
            textAlign: "left",
            borderStyle: "solid",
            borderWidth: 1,
        },
        datos1: {
            backgroundColor: '#D3D3D3',
            color: "black",
            width: '70%',
            height: '15%',
            fontSize: 12,
            margin: '10px',
            padding: '6px',
            textAlign: "left",
        },
        fecha: {
            backgroundColor: 'white',
            color: 'black',
            fontSize: 12,
            textAlign: "right",
            margin: '10px',
            padding: '6px',
        },
        titulo: {
            backgroundColor: 'white',
            color: 'black',
            fontSize: 12,
            textAlign: "left",
            margin: '10px',
            padding: '6px',
        }
    });
    
    // Create Document Component
    const MyDocument = () => (
        <Document>
        <Page size="LEGAL" style={styles.page}>
            <View style={styles.section}> 
                <View style={styles.view}>
                    <Image style={styles.image}  src={logodesydl} alt="images" />
                </View>
                <View style={styles.view}>
                    <Image style={styles.image}  src={gobtuc} alt="images" />
                </View>
            </View>
            <View style={styles.fecha}>
                    <Text>San Miguel de Tucumán, { " " + moment().format('LL') } </Text>
            </View>
            <View style={styles.titulo}>
                    <Text>Datos del Beneficiario: </Text>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Nombre: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.apellido + ", " + benef.nombre } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Linea: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.pmo } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Estado: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.id_estado == 0 ? 'Sin Ingreso' : benef.sub_estado } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Departamento: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.nombredepa } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Municipio: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.nombremuni } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Localidad: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.nombreloca } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Domicilio: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.dom_calle + " " + benef.dom_num } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Telefono: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.tel } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Actividad: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.actividad } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Sub Actividad: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.subacti } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Nombre UP: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.nombreup } </Text>
                </View>
            </View>
            <View style={styles.section}> 
                <View style={styles.etiqueta}>
                    <Text>Fecha de Ejec.: </Text>
                </View>
                <View style={styles.datos} >
                    <Text>{ benef.f_ejecucio ? moment(benef.f_ejecucio).format('DD-MM-YYYY') : null } </Text>
                </View>
            </View>
        </Page>
        </Document>
    );

    return (
    <Card>
        <Card.Header>
            <Accordion.Toggle as={Button} style={{width: "100%"}} variant="light" eventKey={index.toString()}>
                <Row>
                <Col sm={2}>{ benef.pmo }</Col>
                {/* <Col sm={2}>{ moment(benef.created_at).format('DD-MM-YYYY') }</Col> */}
                <Col sm={5} className="text-left" >{ "Nombre: "+benef.apellido + ", " + benef.nombre }</Col>
                <Col sm={2} className="text-left">{ "DNI: "+benef.documento }</Col>
                {/* <Col sm={2}><div class="d-flex justify-content-end"> { benef.cantotal.toFixed(0) }</div></Col> */}
                {/* <Col sm={2}><div class="d-flex justify-content-end">$ { benef.total.toFixed(2) }</div></Col> */}
                <Col sm={3}><div class="d-flex justify-content-end"> { benef.id_estado == 0 ?  <Button variant="danger" className="badge">Sin Ingreso</Button> : <Button variant="success" className="badge"> { benef.sub_estado } </Button> }</div></Col>
                </Row>
            </Accordion.Toggle>
        </Card.Header>
        <Accordion.Collapse eventKey={index.toString()}>
        <Card.Body>
            <Row className="mt-2">
                <Col sm="2" >
                    <MaskedFormControl type='text' name='cuit' disabled value = { benef.cuit} mask='11-11111111-1'  />
                    <Form.Text className="text-muted pl-1">
                    Cuit
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.fechanaci ? moment(benef.fechanaci).format('DD-MM-YYYY') : null } />
                    <Form.Text className="text-muted pl-1" >
                    Fecha Nacimiento
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.genero } />
                    <Form.Text className="text-muted pl-1" >
                    Genero
                    </Form.Text>
                </Col>
                <Col sm="3" >
                    <Form.Control type="text" disabled value = { benef.tel } />
                    <Form.Text className="text-muted pl-1" >
                    Telefono
                    </Form.Text>
                </Col>
                <Col sm="3" >
                    <Form.Control type="text" disabled value = { benef.edunivel } />
                    <Form.Text className="text-muted pl-1" >
                    Educacion nivel
                    </Form.Text>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col sm="3">
                    <Form.Control type="text" disabled value = { benef.nombredepa } />
                    <Form.Text className="text-muted pl-1" >
                    Departamento
                    </Form.Text>
                </Col>
                <Col sm="5">
                    <Form.Control type="text" disabled value = { benef.nombremuni } />
                    <Form.Text className="text-muted pl-1" >
                    Municipio
                    </Form.Text>
                </Col>
                <Col sm="4">
                    <Form.Control type="text" disabled value = { benef.nombreloca } />
                    <Form.Text className="text-muted pl-1" >
                    Localidad
                    </Form.Text>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col sm="8" >
                    <Form.Control type="text" disabled value = { benef.dom_calle } />
                    <Form.Text className="text-muted pl-1" >
                    Domicilio
                    </Form.Text>
                </Col>
                <Col sm="4" >
                    <Form.Control type="text" disabled value = { benef.dom_num } />
                    <Form.Text className="text-muted pl-1" >
                    Numero
                    </Form.Text>
                </Col>
            </Row>
            <Row  className="mt-2">
                    <Col sm={12}>
                        <hr />
                    </ Col>
            </Row>
            <Row className="mt-2">
                <Col sm="6" >
                    <Form.Control type="text" disabled value = { benef.actividad } />
                    <Form.Text className="text-muted pl-1" >
                    Actividad
                    </Form.Text>
                </Col>
                <Col sm="6" >
                    <Form.Control type="text" disabled value = { benef.subacti } />
                    <Form.Text className="text-muted pl-1" >
                    Sub Actividad
                    </Form.Text>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col sm="6" >
                    <Form.Control type="text" disabled value = { benef.nombreup } />
                    <Form.Text className="text-muted pl-1" >
                    Nombre Unidad Productiva
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.f_ingreso ? moment(benef.f_ingreso).format('DD-MM-YYYY') : null } />
                    <Form.Text className="text-muted pl-1" >
                    Fecha de Ingreso
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.f_ejecucio ? moment(benef.f_ejecucio).format('DD-MM-YYYY') : null } />
                    <Form.Text className="text-muted pl-1" >
                    Fecha de Ejecucion
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.num_intern } />
                    <Form.Text className="text-muted pl-1" >
                    Numero Interno
                    </Form.Text>
                </Col>
            </Row>
            <Row  className="mt-2">
                    <Col sm={12}>
                        <hr />
                    </ Col>
            </Row>
            <Row className="mt-2">
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.num_proyec } />
                    <Form.Text className="text-muted pl-1" >
                    Numero de Proyecto
                    </Form.Text>
                </Col>
                <Col sm="8" >
                    <Form.Control type="text" disabled value = { benef.nombreproy } />
                    <Form.Text className="text-muted pl-1" >
                    Nombre de Proyecto
                    </Form.Text>
                </Col>
                <Col sm="2" >
                    <Form.Control type="text" disabled value = { benef.monto } />
                    <Form.Text className="text-muted pl-1" >
                    Monto
                    </Form.Text>
                </Col>
            </Row>
            <Row  className="mt-2">
                <Col sm={12}>
{/*                     <PDFDownloadLink as={Button} document={<MyDocument />} fileName="somename.pdf">
                    {
                    ({loading}) => loading ? 'Loading' : 'Download'
                    }     
                    </PDFDownloadLink> */}

                    <Button as={PDFDownloadLink} variant="warning" document={<MyDocument />} fileName= {benef.apellido + " - DNI: " + benef.documento } >
                    {
                    ({loading}) => loading ? 'Cargando...' : 'Descargar PDF'
                    }    
                    </Button>
                </Col>
            </Row> 
        </Card.Body>
        </Accordion.Collapse>
    </Card>
    );
}

export default BeneficiarioDetalle;