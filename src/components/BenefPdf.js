import React from 'react';
import { usePDF, Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import { PDFViewer } from '@react-pdf/renderer';
import { useParams } from 'react-router-dom';

function BenefPdf(props) {
  // Obtenemos el parametro en esta variable
  //const benef = useParams();
  const state  = props.location.state;
  console.log('ingreso');
  console.log(props.location.state);
    // Create styles
    const styles = StyleSheet.create({
        page: {
        flexDirection: 'row',
        backgroundColor: '#E4E4E4'
        },
        section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
        }
    });
    
    // Create Document Component
    const MyDocument = () => (
        <Document>
        <Page size="A4" style={styles.page}>
            <View style={styles.section}>
            <Text>Section #1 {state} </Text>
            </View>
            <View style={styles.section}>
            <Text>Section #2</Text>
            </View>
        </Page>
        </Document>
    );
    return (
      <PDFViewer>
        <MyDocument />
      </PDFViewer>
    );

}

export default BenefPdf;





const App = () => {

}