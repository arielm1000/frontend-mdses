import React, {Fragment} from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const Navigation = () => {
  return (
    <Fragment>
    <Navbar collapseOnSelect expand="lg" bg="warning">
      <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto" >
          <Nav.Link as={Link} to="/subirbene" className="text-dark">Subir y Actualizar Beneficiarios</Nav.Link>
          {/* <Nav.Link as={Link} to="/subirtirillas" className="text-dark">Actualizar Tirillas</Nav.Link> */}
          <NavDropdown title="Actualizar Tirillas" id="basic-nav-dropdown" expand="lg" className="text-dark" >
              <NavDropdown.Item as={Link} to="/buscarbene"  className="text-dark">Por Documento</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/buscarbenename"  className="text-dark">Por Apellido</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link as={Link} to="/registro" className="text-dark">Registrar Usuarios</Nav.Link>
          {/* <Nav.Link as={Link} to="/buscarbene" className="text-dark">Buscar Beneficiarios</Nav.Link> */}
          <NavDropdown title="Buscar Beneficiarios" id="basic-nav-dropdown" expand="lg" className="text-dark" >
              <NavDropdown.Item as={Link} to="/buscarbene"  className="text-dark">Por Documento</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/buscarbenename"  className="text-dark">Por Apellido</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>    
    </Fragment>
  );
};

export default Navigation;
