import React, { useState, Fragment, useContext, useEffect } from 'react'
import { Container, Row, Col, Form, Button, Card, Alert } from 'react-bootstrap';
import NavigationHeader from "./NavigationHeader";
import Footer from "../Footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import AuthContext from "../../context/authContext";
import AlertaContext from "../../alertas/alertaContext";

function Login(props) {
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;
    // Obtener el context de Autenticación
     const authContext = useContext(AuthContext);
    const {
      mensaje,
      iniciarSesion,
      usuario,
    } = authContext; 

    //satate del usuario
    const [user, setUser] = useState({
        email: "",
        password: "",
    }); 
    // Extraemos del usuario
    const { email, password } = user;
    
    // Cuando cambian los campos
    const onChangeUsuario = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    };
    
    // Cuando el usuario quiere iniciar sesión
    const onSubmitLogin = e => {
        e.preventDefault();
        //valido los campos email y password
        if(email.length < 1 || password.length < 6)
        {   mostrarAlerta("Debe Completar los Campos.", 'warning');
            return;}
        iniciarSesion({ email, password });
           
    }
    //para direccionar 
    useEffect(() => {
        if(usuario){
            props.history.push('/');
        }
    }, [usuario]);
        
    return (
        <Fragment>
        <NavigationHeader />
        <Container>
            <Row className="mt-5">
                <Col xs={12} sm={8} md={6} className="mx-auto">
                    {alerta ? (
                        <Alert variant={alerta.alerta} className="mt-2 text-center">
                        {alerta.msg}
                        </Alert>
                    ) : null}
                    {mensaje ? (
                        <Alert variant={'primary'} className="mt-2 text-center">
                        {mensaje}
                        </Alert>
                    ) : null}
                    <Card bg="light">
                        <Card.Header className="d-flex justify-content-center"><FontAwesomeIcon icon={faUser} color="#ffc107" size="5x" /></Card.Header>
                        <Card.Body>
                            <Form onSubmit={onSubmitLogin}>
                                <Form.Group controlId="formLoginEmail">
                                    <Form.Label>
                                        Email
                                    </Form.Label>
                                    <Form.Control 
                                        type="email"
                                        name="email"
                                        placeholder="Ingrese su email"
                                        onChange={onChangeUsuario}
                                        value={email}
                                    />
                                </Form.Group>
                                <Form.Group controlId="formLoginPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control 
                                        type="password"
                                        name="password"
                                        placeholder="Ingrese su password"
                                        onChange={onChangeUsuario}
                                        value={password}
                                    />
                                </Form.Group>
                                <Button 
                                    className="mr-3"
                                    variant="warning"
                                    type="submit">Iniciar Sesión
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>                
            </Row>
        </Container>
        <Footer />
        </Fragment>
    )
}

export default Login;
