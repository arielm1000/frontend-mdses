import React, {Fragment} from 'react';
import NavigationHeader from "./NavigationHeader";
import Navigation from "./Navigation";
import Footer from "../Footer";
import {Button, Row, Alert} from "react-bootstrap";

function Registro(props) {
    return (
        <Fragment>
            <NavigationHeader />
            <Navigation />
            <Alert variant="warning">
             Registrar Usuarios
            </Alert>
                <Row className="justify-content-center mt-2">
                    <Button
                        variant="warning"
                        className="mt-5"> Registrar Usuarios

                    </Button>
                </Row>
            <Footer />
        </Fragment>
    );
}

export default Registro;