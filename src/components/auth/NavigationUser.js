import React, {Fragment} from "react";
import { Navbar, Nav, Button, Badge, Form, FormControl, Image  } from "react-bootstrap";
import { Link } from "react-router-dom";

function NavigationUser() {
    return (
        <Fragment>
        <Navbar bg="primary" variant="dark" className="block-example border-top border-info">
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link to="/buscarbene" className="nav-link">
                Buscar Beneficiarios
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        </Fragment>
    )
}

export default NavigationUser;
