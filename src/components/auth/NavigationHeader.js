import React, {useContext}  from 'react'
import { Nav, Navbar, Container, Button, Row, Badge, Form, FormControl } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faPhoneAlt, faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import AuthContext from "../../context/authContext";

function NavigationHeader() {
// Obtener el context de Autenticación
const authenticationContext = useContext(AuthContext);
const { usuario, cerrarSesion } = authenticationContext;
// fn para cerrar cesion
const cerrar = () => {
  cerrarSesion();
}
return (
    <Navbar bg="warning" variant="dark" expand="lg">
      <Container>
        <Link to="/" className="navbar-brand text-dark">MDS Economia Social</Link>
        <Nav className="mr-auto">
            <Nav.Link className="text-dark"><FontAwesomeIcon icon={faPhoneAlt} size="1x" /> +54 (0381) 4222222</Nav.Link>
        </Nav>
        {!usuario 
        ?
        <Nav className="ml-auto">
            <Link to="/Login" className="nav-link"><FontAwesomeIcon icon={faUser} size="1x" /> Ingresar</Link>
        </Nav>
        :
        <Row>
        <Navbar.Brand>
        <Navbar.Text className="mr-1">Hola</Navbar.Text>
        { 
            usuario.nombre 
        }
        </Navbar.Brand>
        <Nav className="ml-auto">
            <Button as={Link} 
                variant="outline-light" 
                onClick={() => cerrar()} 
                className="pt-3"
                Link to="/">
            Cerrar Sesión</Button>
        </Nav>
        </Row>
        }
      </Container>
    </Navbar>
)
}

export default NavigationHeader;
