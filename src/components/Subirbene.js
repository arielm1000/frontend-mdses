import React, {Fragment, useState} from 'react';
import NavigationHeader from '../components/auth/NavigationHeader';
import Navigation from '../components/auth/Navigation';
import Footer from './Footer';
import {Button, Row, Alert, Container, Form, Spinner} from 'react-bootstrap';

function Subirbene(props) {
    //state para spinner
    const [cargando, setCargando] = useState(false);
    //state para el archivo filedbf
    const [file1, setFile1] = useState({file:""})
    //state para spinner subir
    const [subir, setSubir] = useState(false);

    //funcion para actualizar todos los Beneficiarios
    const actBeneficiarios = async () => {
        //alert("entro")
        setCargando(true);
        //const solicitud = await fetch('http://localhost:4000/api/actbeneficiarios/', {
        const solicitud = await fetch('https://back-mdsestuc.herokuapp.com/api/actbeneficiarios/', {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        alert(respuesta.msg);
        console.log(respuesta.msg);
        setCargando(false);
    }
    // cuando sube el filedbf
    const onChangeFiledbf = async e => {
        if(e.target.files[0]){
            if(e.target.files[0].size > 20971520){
                // 20971520 = 20MB
                // 5242880 = 5MB
                // 4194304 = 4MB
                e.target.value = null;
                alert('El archivo es demasiado grande.');
                setFile1({
                    file: null
                });
                return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onloadend = () => {
                setFile1({file: reader.result});
                
            };

            const formData = new FormData();
            formData.append('file', e.target.files[0]);
            console.log( "este el target despues del fetch ", e.target.files[0])
        } else {
            alert('No se ha seleccionado un Archivo.');
            setFile1({
                file: null
            });
        }
    };
    
    const actFiledbf = async () => {
        //alert("entro")
        if(file1.file==="" || file1.file=== null){
            alert("No se subio el archivo.");
            return;
        }
        setSubir(true);
        var file = new FormData();
        var data = document.querySelector('input[type="file"]').files[0];
        file.append("file", data);
        for (var value of file.values()) {
            console.log("valores de formdata file antes solicitud ",value);
        }
        
        //const solicitud = await fetch('http://localhost:4000/api/actfiledbf/', {
        const solicitud = await fetch('https://back-mdsestuc.herokuapp.com/api/actfiledbf/', {
            method: 'POST',
            body: file,
            headers:{
                //'Content-Type': 'multipart/form-data; boundary=--------------------------784096586197410423836092',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        alert(respuesta.msg);
        console.log(respuesta.msg);
        setSubir(false);
    }    

    return (
        <Fragment>
            <NavigationHeader/>
            <Navigation />
            <Alert variant="warning">
            Subir y Actualizar Beneficiarios
            </Alert>
            <Container>
                <Row className ="justify-content-center">
                    <Alert variant="warning">
                    Seleccione el archivo con los Beneficiarios a Actualizar...
                    </Alert>
                </Row>
                <Row className ="justify-content-center">                
                    <Form.Group controlId="filedbf" className="mt-2">
                        <Form.File encType="multipart/form-data"
                            id="filedbf"
                            name="filedbf"
                            onChange={onChangeFiledbf}
                        />
                    </Form.Group>
                </Row>
                <Row className ="justify-content-center m-4">
                    <h5>Subir Archivo al Servidor...</h5>
                </Row>
                <Row className ="justify-content-center">
                    {!subir ?
                    <Button 
                            variant="warning"
                            className = "m-2" 
                            onClick={()=>actFiledbf()}
                        >Subir Archivo
                    </Button>
                    :
                    <Button variant="warning" disabled>
                        <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            />
                        <span> Subiendo...</span>
                    </Button>                
                    }
                </Row>
                <Row className ="justify-content-center m-4">
                    <h5>Actualizar Beneficiarios en el Servidor...</h5>
                </Row>
                <Row className ="justify-content-center">
                    {
                    !cargando ? 
                        <Button 
                            variant="warning"
                            className = "m-2" 
                            onClick={()=>actBeneficiarios()}
                        >Actualizar Beneficiarios
                        </Button>
                        :
                        <Button variant="warning" disabled>
                            <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            />
                            <span> Actualizando...</span>
                        </Button>
                    }
                </Row>
            </Container>
            <Footer/>
        </Fragment>
    );
}

export default Subirbene;