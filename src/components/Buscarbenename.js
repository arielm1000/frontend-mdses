import React, {Fragment, useState} from 'react';
import NavigationHeader from './auth/NavigationHeader';
import Navigation from './auth/Navigation';
import Footer from './Footer';
import BeneficiarioDetalle from './BeneficiarioDetalle';
import {Button, Row, Alert, Container, Form, Col, Accordion} from 'react-bootstrap';

function Buscarbenename(props) {
    //state de cada beneficiario para modificar 
/*     const [beneficiario, setBeneficiario] = useState({   
    }); */
    //state del documento a buscar
    const [nombre, setNombre] = useState({namebuscar: "", name: ""})
    const {namebuscar, name} = nombre;
    //state de las paginas
    const [page, setPage] = useState({page: 0, limit: 6, totalPages: 0});
    //state que contiene los beneficiarios
    const [beneficiarios, setBeneficiarios] = useState([]);
    //state de carga
    const [carga, setCarga] = useState(false);
    //state para el nuevo listado
    //const [newlist, setNewlist] = useState(false);

    // cuando cambian los campos
    const onChangeBuscarName = e => {
        setNombre({
            ...nombre,
            [e.target.name]: e.target.value
        });
    };
    //funcion para agregar mas beneficiarios `Field names: ${dbf.fields.map(f => f.name).join(', ')}`
    const cargarPage = async () => {
        //alert(docubuscar);
        //valido el campo docubuscar
        if(namebuscar.length < 1 || namebuscar.length === "")
        {   alert("Debe Ingresar el Apellido.", 'warning');
            return;}
        setCarga(true);
        //setBeneficiario([]);
        const buscar = async () => {
            const solicitud = await fetch(`https://back-mdsestuc.herokuapp.com/api/benebuscarname?porpagina=${page.limit}&pagina=${page.page+1}`, {
            //const solicitud = await fetch(`http://localhost:4000/api/benebuscarname?porpagina=${page.limit}&pagina=${page.page+1}`, {
            /*const solicitud = await fetch(`http://10.100.111.112:4000/api/benebuscardocu?porpagina=${page.limit}&pagina=${page.page}`, {*/
                method: 'GET',
                headers:{
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem('token'),
                    'namebuscar': nombre.namebuscar,
                    'name': nombre.name
                } 
            });
            const respuesta = await solicitud.json();
            if (solicitud.ok) {
                const {docs, ...resto} = respuesta.beneficiario;
                //setBeneficiarios([...docs]);
                setBeneficiarios([ ...beneficiarios, ...docs]);
                setPage(resto)
            } else {
                alert(respuesta.msg);
            }
            //setCarga(false);
        }
        buscar();
    }
    const inicializar = async () =>{
        //await setNewlist(false);
        await setCarga(false);
        page.page=0;
        page.limit=6;
        page.totalPages=0;
        await setPage(page);
        beneficiarios.slice(0, beneficiarios.length);
        await setBeneficiarios([]);
        setNombre({namebuscar: "", name: ""});
    }

    return (
        <Fragment>
            <NavigationHeader/>
            <Navigation />
            <Alert variant="warning">
            Buscar Beneficiarios por Apellido
            </Alert>
            <Container>
                <Row className="mt-5">
                    <Form className="w-100 mb-5" >
                        <Row className="justify-content-center">
                            <Col sm={3}>
                                <Form.Group controlId="formnamebuscar">
                                    <Form.Control 
                                        type="text"
                                        name="namebuscar"
                                        required
                                        placeholder="Ingrese el Apellido"
                                        onChange={onChangeBuscarName}
                                        value={namebuscar}
                                    />
                                </Form.Group>
                            </Col>
                            <Col sm={3}>
                                <Form.Group controlId="formname">
                                    <Form.Control 
                                        type="text"
                                        name="name"
                                        required
                                        placeholder="Ingrese el Nombre"
                                        onChange={onChangeBuscarName}
                                        value={name}
                                    />
                                </Form.Group>
                            </Col>
                            <Col sm={3}>
                                {!carga ?
                                    <Button 
                                        variant="warning"
                                        block
                                        onClick={() => cargarPage(namebuscar)}
                                    > Buscar Beneficiario
                                    </Button>
                                :
                                    <Button 
                                        variant="warning"
                                        block
                                        onClick={() => inicializar(namebuscar)}
                                    > Otro Listado
                                    </Button>
                                }
                            </Col>
                        </Row>
                    </Form>
                </Row>


                <Row>
                    <Col>
                    <Accordion>
                    {
                        beneficiarios.map((benef, index) => <BeneficiarioDetalle 
                            benef={benef} 
                            index={index}
                            />)
                    }
                    </Accordion>
                    </Col>
                </Row>

                <Row className="justify-content-center mt-3">
                    {
                    page.page < page.totalPages ? 
                    <Button 
                        variant="warning"
                        onClick = {() => cargarPage()}
                    > CARGAR MAS
                    </Button>
                    : null
                    }
                </Row>


            </Container>
            <Footer/>
        </Fragment>
    );
}

export default Buscarbenename;