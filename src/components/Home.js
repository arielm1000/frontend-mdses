import React, { Fragment, useContext } from 'react';
import NavigationHeader from "./auth/NavigationHeader";
import Footer from "./Footer";
import CarruselPrincipal from "./CarruselPrincipal";
import AuthContext from "../context/authContext";
import Navigation from "../components/auth/Navigation"
 
export default function Home() {
    const authContext = useContext(AuthContext);
    const { usuario } = authContext; 

 return (
    <Fragment>
        <NavigationHeader />
        { !usuario ? null : usuario.rol === "Admin" ? <Navigation /> : null }
        <CarruselPrincipal />
        <Footer />
    </Fragment>
 )
}
