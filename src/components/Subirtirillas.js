import React, {Fragment} from 'react';
import NavigationHeader from "./auth/NavigationHeader";
import Navigation from "./auth/Navigation";
import Footer from "./Footer";
import {Button, Row, Alert} from "react-bootstrap";

function Subirtirillas(props) {
    return (
        <Fragment>
            <NavigationHeader />
            <Navigation />
            <Alert variant="warning">
            Subir y Actualizar Tirillas
            </Alert>
                <Row className="justify-content-center mt-2">
                    <Button
                        variant="warning"
                        className="mt-5"> Subir y Actualizar Tirillas

                    </Button>
                </Row>
            <Footer />
        </Fragment>
    );
}

export default Subirtirillas;