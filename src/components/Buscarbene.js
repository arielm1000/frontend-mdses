import React, {Fragment, useState} from 'react';
import NavigationHeader from '../components/auth/NavigationHeader';
import Navigation from '../components/auth/Navigation';
import Footer from './Footer';
import BeneficiarioDetalle from '../components/BeneficiarioDetalle';
import {Button, Row, Alert, Container, Form, Col, Accordion} from 'react-bootstrap';

function Buscarbene(props) {
    //state de cada beneficiario para modificar 
/*     const [beneficiario, setBeneficiario] = useState({   
    }); */
    //state del documento a buscar
    const [documento, setDocumento] = useState({docubuscar: ""})
    const {docubuscar} = documento;
    //state de las paginas
    const [page, setPage] = useState({page: 1, limit: 8});
    //state que contiene los beneficiarios
    const [beneficiarios, setBeneficiarios] = useState([]);
    //state de carga
    const [carga, setCarga] = useState(false);

    // cuando cambian los campos
    const onChangeBuscarDocu = e => {
        setDocumento({
            ...documento,
            [e.target.name]: e.target.value
        });
    };
    //funcion para agregar mas beneficiarios `Field names: ${dbf.fields.map(f => f.name).join(', ')}`
    const cargarPage = async () => {
        //alert(docubuscar);
        //valido el campo docubuscar
        if(docubuscar.length < 1 || docubuscar.length === "")
        {   alert("Debe Ingresar DNI.", 'warning');
            return;}
        setCarga(true);
        setBeneficiarios([]);
        //setBeneficiario([]);
        const buscar = async () => {
            const solicitud = await fetch(`https://back-mdsestuc.herokuapp.com/api/benebuscardocu?porpagina=${page.limit}&pagina=${page.page}`, {
            //const solicitud = await fetch(`http://localhost:4000/api/benebuscardocu?porpagina=${page.limit}&pagina=${page.page}`, {
            /*const solicitud = await fetch(`http://10.100.111.112:4000/api/benebuscardocu?porpagina=${page.limit}&pagina=${page.page}`, {*/
                method: 'GET',
                headers:{
                    'Content-Type': 'application/json',
                    'x-auth-token': localStorage.getItem('token'),
                    'docubuscar': documento.docubuscar
                } 
            });
            const respuesta = await solicitud.json();
            if (solicitud.ok) {
                const {docs, ...resto} = respuesta.beneficiario;
                setBeneficiarios([...docs]);
                //setBeneficiarios([ ...beneficiarios, ...docs]);
                setPage(resto)
            } else {
                alert(respuesta.msg);
            }
            setCarga(false);
        }
        buscar();
    }

    return (
        <Fragment>
            <NavigationHeader/>
            <Navigation />
            <Alert variant="warning">
            Buscar Beneficiarios por Documento
            </Alert>
            <Container>
                <Row className="mt-5">
                    <Form className="w-100 mb-5" >
                        <Row className="justify-content-center">
                            <Col sm={3}>
                                <Form.Group controlId="formdocubuscar">
                                    <Form.Control 
                                        type="number"
                                        name="docubuscar"
                                        required
                                        placeholder="Ingrese DNI"
                                        onChange={onChangeBuscarDocu}
                                        value={docubuscar}
                                    />
                                </Form.Group>
                            </Col>
                            <Col sm={3}>
                                <Button 
                                    variant="warning"
                                    block
                                    onClick={() => cargarPage(docubuscar)}
                                > Buscar Beneficiario
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Row>


                <Row>
                    <Col>
                    <Accordion>
                    {
                        beneficiarios.map((benef, index) => <BeneficiarioDetalle 
                            benef={benef} 
                            index={index}
                            />)
                    }
                    </Accordion>
                    </Col>
                </Row>

{/*                 <Row className="justify-content-center mt-3">
                    {
                    page.page < page.totalPages ? 
                    <Button onClick = {() => ListarPedidos()}>
                        CARGAR MAS
                    </Button>
                    : null
                    }
                </Row> */}


            </Container>
            <Footer/>
        </Fragment>
    );
}

export default Buscarbene;