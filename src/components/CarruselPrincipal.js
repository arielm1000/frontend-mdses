import React from 'react';
import { Carousel } from "react-bootstrap";
import img01 from "./img/img01.JPG";
import img02 from "./img/img02.JPG";
import img03 from "./img/img03.JPG";

function CarruselPrincipal() {
    return (
<Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={img01}
      alt="First slide"
    />
{/*     <Carousel.Caption>
      <h3>First slide label</h3>
      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
    </Carousel.Caption> */}
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={img02}
      alt="Third slide"
    />
{/*     <Carousel.Caption>
      <h3>Second slide label</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption> */}
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={img03}
      alt="Third slide"
    />
{/*     <Carousel.Caption>
      <h3>Third slide label</h3>
      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
    </Carousel.Caption> */}
  </Carousel.Item>
</Carousel>
    )
}

export default CarruselPrincipal
