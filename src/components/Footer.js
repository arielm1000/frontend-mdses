import React from 'react'
import { ListGroup, Container, Row, Col } from 'react-bootstrap';
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faInstagram, faWhatsapp } from '@fortawesome/free-brands-svg-icons'

function Footer() {
  const dia = new Date();

  return (
      <Row className="mt-5">
        <hr style={{width: "100%"}}></hr>
        <Container>
          <Row xs="2" className="justify-content-center">
            <Col>
              <ListGroup variant="flush" className="text-dark">
                <ListGroup.Item >Ministerio de Desarrollo Social Tucumán </ListGroup.Item>
                <ListGroup.Item>Dirección de Economía Social y Desarrollo Local </ListGroup.Item>
                <ListGroup.Item>Saenz Peña 15 Tucumán (C.P. 4000)</ListGroup.Item>
                <ListGroup.Item>Tel.: 0381-4222222 Cel.: 381 5555555</ListGroup.Item>
              </ListGroup>
            </Col>
            <Col className=" d-flex justify-content-center align-items-center px-2 ">
            <a href="https://www.facebook.com/Guinditoystore/" target="_blank" className="ml-3 text-dark"><FontAwesomeIcon icon={faFacebook} size="3x" /></a>
            <a href="https://www.instagram.com/guindi_toystore/" target="_blank" className="ml-3 text-dark"><FontAwesomeIcon icon={faInstagram} size="3x" /></a>
            <a href="https://web.whatsapp.com/" target="_blank" className="ml-3 text-dark"><FontAwesomeIcon icon={faWhatsapp} size="3x" /></a>
            </Col>
          </Row>
          <hr style={{width: "100%"}}></hr>
          <Row className="justify-content-center px-4">
            <p className="text-dark">MDS Copyright &copy; {dia.getFullYear()} Todos los derechos reservados</p> 
          </Row>
        </Container>
        
    </Row>
  )
}

export default Footer;
