import React, { useReducer } from 'react';
import AuthContext from './authContext';
import AuthReducer from './authReducer';

/* import clienteAxios from '../../config/axios';
import tokenAuth from '../../config/tokenAuth'; */

import { 
    REGISTRO_EXITOSO,
    REGISTRO_ERROR,
    OBTENER_USUARIO,
    LOGIN_EXITOSO,
    LOGIN_ERROR,
    CERRAR_SESION
} from '../actionstypes/index';

const AuthState = props => {
    //console.log(JSON.parse(localStorage.getItem('usuario')))
    const initialState = {
        token: localStorage.getItem('token'),
        autenticado: localStorage.getItem('token') ? true : false,
        usuario: localStorage.getItem('usuario') ?  JSON.parse(localStorage.getItem('usuario')) : null,
        mensaje: null
    }

    const [ state, dispatch ] = useReducer(AuthReducer, initialState);

    const registrarUsuario = async datos => {
            console.log(datos);
            const solicitud = await fetch('https://back-guindi.herokuapp.com/api/usuarios', {
                method: 'POST',
                body: JSON.stringify(datos),
                headers:{
                    'Content-Type': 'application/json'
                }
            });

            const respuesta = await solicitud.json();

            if(solicitud.ok){
                dispatch({
                    type: REGISTRO_EXITOSO,
                    payload: respuesta
                });
            } else {
                dispatch({
                    type: REGISTRO_ERROR,
                    payload: respuesta.msg
                });
            }
    }

    // Retorna el usuario autenticado
    const usuarioAutenticado = async () => {
        //const token = localStorage.getItem('token');
        // console.log(token);
        
/*         if(token) {
            tokenAuth(token);
        }  */
        try {
            //const respuesta = await clienteAxios.get('/api/auth');
            // console.log("Respuesta",respuesta.data.usuario); 
            let respuesta =""     
            dispatch({
                type: OBTENER_USUARIO,
                payload: respuesta.data.usuario
            });

        } catch (error) {
            dispatch({
                type: LOGIN_ERROR
            })
        }
    }

    // Cuando el usuario inicia sesión
            //const solicitud = await fetch('http://localhost:4000/api/auth/', {
    const iniciarSesion = async datos => {
        const solicitud = await fetch('https://back-mdsestuc.herokuapp.com/api/auth/', {
            method: 'POST',
            body: JSON.stringify(datos),
            headers:{
                'Content-Type': 'application/json' },
        });

        const respuesta = await solicitud.json();
       
        if(solicitud.ok){
            dispatch({
                type: LOGIN_EXITOSO,
                payload: respuesta
            });
        } else {
            dispatch({
                type: LOGIN_ERROR,
                payload: respuesta.msg
            });
        }
    }

    // Cierra la sesión del usuario
    const cerrarSesion = () => {
        dispatch({
            type: CERRAR_SESION
        })
    }

    return(
        <AuthContext.Provider
            value={{
                token: state.token,
                autenticado: state.autenticado,
                usuario: state.usuario,
                mensaje: state.mensaje,
                cargando: state.cargando,
                is_admin: state.is_admin,
                registrarUsuario,
                iniciarSesion,
                usuarioAutenticado,
                cerrarSesion
            }}
        >{props.children}

        </AuthContext.Provider>
    )
}
export default AuthState;
