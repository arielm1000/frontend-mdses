import {
    REGISTRO_EXITOSO,
    REGISTRO_ERROR,
    LOGIN_EXITOSO,
    LOGIN_ERROR,
    CERRAR_SESION,
    OBTENER_USUARIO,
  } from "../actionstypes/index";
  
  export default (state, action) => {
    switch (action.type) {
      case REGISTRO_EXITOSO:
        return {
            ...state,
            mensaje: action.payload.msg
        }
      case LOGIN_EXITOSO:
        localStorage.setItem('token',action.payload.token);
        localStorage.setItem('usuario',JSON.stringify(action.payload.usuario));
        return {
            ...state,
            token: action.payload.token,
            autenticado: true,
            usuario: action.payload.usuario,
            mensaje: null
        };
      case OBTENER_USUARIO:
        return {
          ...state,
          autenticado: true,
          usuario: action.payload,
          cargando: false,
          is_admin: action.payload.is_admin,
        };
      case CERRAR_SESION:
        localStorage.removeItem('token');
        localStorage.removeItem('usuario');
        return {
            ...state,
            token: null,
            autenticado: false,
            usuario: null,
            mensaje: null
        }
      case LOGIN_ERROR:
      case REGISTRO_ERROR:
        localStorage.removeItem("token");
        return {
          ...state,
          token: null,
          usuario: null,
          autenticado: null,
          mensaje: action.payload,
          cargando: false,
        };
  
      default:
        return state;
    }
  };
  